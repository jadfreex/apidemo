package net.jadfreex.api.config;

import net.jadfreex.test.model.Shop;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 *
 * @author jadiazc
 */
@ComponentScan(basePackages = {"net.jadfreex"})
public class AppConfig {

    @Bean
    public Shop shop() {
        Shop shop = new Shop();
        shop.setName("test");
        shop.setStaffName(new String[]{
            "xxx",
            "yyy"
        });
        return shop;
    }

}
