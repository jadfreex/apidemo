package net.jadfreex.api.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 *
 * @author jadiazc
 */
@EnableWebMvc
@ComponentScan(basePackages = {"net.jadfreex"})
public class WebConfig extends WebMvcConfigurerAdapter {

    //@Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new HandlerInterceptor() {

            @Override
            public boolean preHandle(HttpServletRequest request,
                    HttpServletResponse response, Object o)  {

                return true;
            }

            @Override
            public void postHandle(HttpServletRequest request,
                    HttpServletResponse response, Object o,
                    ModelAndView modelView)  {

                response.setHeader("Pragma", "No-cache");
                response.setHeader("Cache-Control", "no-cache,no-store,max-age=0");
                response.setDateHeader("Expires", 1);
            }

            @Override
            public void afterCompletion(HttpServletRequest request,
                    HttpServletResponse response, Object o,
                    Exception exception)  {}
        });
    }

}
