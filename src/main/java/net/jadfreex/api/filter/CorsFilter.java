package net.jadfreex.api.filter;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpMethod;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 *
 * Filtro de CORS.
 *
 * @author José Antonio Díaz Castro
 */
public class CorsFilter extends OncePerRequestFilter {

    @Override
    public void doFilterInternal(HttpServletRequest request,
            HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods",
                "OPTIONS, GET, POST, PUT, DELETE");
        response.addHeader("Access-Control-Allow-Headers",
                "Cache-Control, Pragma, Origin, Accept, Authorization, Content-Type, X-Requested-With");
        response.addHeader("Access-Control-Max-Age", "3600");

        String metodo = request.getMethod();

        if(!metodo.equals(HttpMethod.OPTIONS)) {
            filterChain.doFilter(request, response);
        }
    }
}
