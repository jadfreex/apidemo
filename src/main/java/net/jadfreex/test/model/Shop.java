package net.jadfreex.test.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jadiazc
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Shop implements Serializable {

    @XmlElement
    private String name;
    @XmlElement
    private String staffName[];

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getStaffName() {
        return staffName;
    }

    public void setStaffName(String[] staffName) {
        this.staffName = staffName;
    }

}
