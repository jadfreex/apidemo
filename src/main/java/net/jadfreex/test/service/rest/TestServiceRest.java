package net.jadfreex.test.service.rest;

import net.jadfreex.test.model.Shop;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jadiazc
 */
@RestController
@RequestMapping("/api/v1")
public class TestServiceRest {

    @Autowired
    private Shop shop;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> get() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("status", "on");
        map.put("version", 1);
        return ResponseEntity.ok().body(map);
    }

    @RequestMapping(value = "test",
            method = RequestMethod.GET)
    public ResponseEntity<Shop> getTest() {
        return ResponseEntity.ok().body(shop);
    }

}
